
# Overview

Eureka Server is a discovery server containing a registry of services and a REST API that can be used to register a service, de-register a service, and discover the location of other services.

References:
 * https://spring.io/guides/gs/service-registration-and-discovery/
 * https://github.com/Netflix/eureka


### Steps to Run Service Registry

1. Run ``./mvnw``
   ```
   ----------------------------------------------------------
        Application 'eureka-server' is running! Access URLs:
        Local:          http://localhost:8761/
        External:       http://192.168.1.101:8761/
        Profile(s):     [dev]
   ----------------------------------------------------------
   ```

1. Go to http://localhost:8761/ and the following should appear
   ![Eureka Service Registry](assets/eureka-service-registry.png)